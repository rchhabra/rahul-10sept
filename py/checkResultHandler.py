import time
import json
import logging
import templateDataManager
from httpRequestHandlers import BaseRequestHandler
from configurationManager import questionDataConfiguration, teamDataConfiguration
from configurationManager import applicationConfiguration
from SQLiteUtil import SQLiteUtil
import Team
import Question

class CheckResultHandler(BaseRequestHandler):
    sqlUtil = SQLiteUtil()
    def checkTeamToken(self):

        _userInputToken = self.request.get('token')
        '''sqlUtil = SQLiteUtil()'''
        team=sqlUtil.getTeamDetailsByToken(_userInputToken)

        _isSuccess = 'false'
        result = {}
        if team:
            logging.debug('Team found\n%s' % team.get_team_name())
            _isSuccess = 'true'
            print 'Team Details'
            print(team.get_team_name() )
            print(team.get_team_token() )
            print(team.get_team_score() )
            print(team.get_team_current_question() )
            print(team.get_team_total_time_taken() )
            result['_teamName'] = team.get_team_name()
            result['_currentQuestion'] = team.get_team_current_question()
        else:
            logging.debug('Team not found')
            print 'Team Not found'

        result["_isSuccess"]=_isSuccess
        logging.debug('Result is %s'%result)

        self.response.out.write(json.dumps(result))

    def checkUserSolution(self,teamToken):
        _userSolution = self.request.get('usersolution')
        _userQuestionId = self.request.get('_current_question_id')
        result={}
        _isSuccess = 'true'
        current_question_id = int(_userQuestionId)
        if _userQuestionId and int(_userQuestionId) !=-1:
            logging.debug('checkUserSolution: check answer in database')
            _actualSolution = sqlUtil.getAnswerByIdQuery(_userQuestionId)
            if _actualSolution[0] == _userSolution:
                current_question_id = current_question_id +1
                submit_timestamp = self.request.get('submit_timestamp')
                sqlUtil.updateTeamPoints(teamToken,submit_timestamp)
            else:
                _isSuccess = 'false'

        else:
            logging.debug('checkUserSolution: load question')
            qid = sqlUtil.getCurrentQuestionOfTeamQuery(teamToken)
            current_question_id = qid[0]

            logging.debug('checkUserSolution:_userQuestionId %s' %qid)
        _questionAvailable = 'true'
        print("totalQuestions:", applicationConfiguration['totalQuestions'])
        if int(current_question_id) <= int(applicationConfiguration['totalQuestions']):
            question = sqlUtil.getQuestionByIdQuery(current_question_id)
            if question:
                result['_question']=question.get_question()
                print question.get_question()
                result['_testCasesPath']=question.get_test_case_file_path()
                result['_testcase']=question.get_test_case_file_path()
        else:
            _questionAvailable = 'false'
        result["_questionAvailable"]=_questionAvailable

        result['_current_question_id'] = current_question_id

        result["_isSuccess"]=_isSuccess
        print result
        logging.debug('result %s' %result)

        self.response.out.write(json.dumps(result))

    def get(self):
        global sqlUtil
        sqlUtil = SQLiteUtil()
        _request = self.request.get('requestType')

        _userInputToken = self.request.get('teamCode')
        print("_userInputToken is %s"%_userInputToken)

        _checkTeamTokenRequest = 'userValidation'
        _checkUserSolutionRequest = 'checkUserSolution'

        if _request == _checkTeamTokenRequest:
            self.checkTeamToken()
        elif _request == _checkUserSolutionRequest:
            self.checkUserSolution(_userInputToken)
        else:
            logging.debug("Incorrect ajax request")


        '''_userSol = self.request.get('usersolution')
        _actualSol = 'rahul'
        result = _sol
        phonebook = {"Andrew Parson":"8806336", \
                        "Emily Everett":'6784346', "Peter Power":'7658344', \
                        "Lewis Lame":'1122345'}
        _isSuccess = 'true'

        if _userSol == _actualSol:
            _isSuccess = 'true'
        else:
            _isSuccess = 'false'

        _nextQ ="none"
        if _isSuccess:
            _nextQ='next question is At W3Schools you will find all the Web-building tutorials you need, from basic HTML to advanced XML, SQL, ASP, and PHP. '

        phonebook = {"_request":_request,"_isSuccess":_isSuccess,"_nextQ":_nextQ, \
                        "E":"6784346"}


        #self.response.out.write(str(result))
        self.response.out.write(json.dumps(phonebook))'''

        #print
         # Render the page
        #self.render_response('questionsPage.html', **templateData)
        '''logging.debug("In QuestionsPageHandler::get()...")

        templateData = templateDataManager.getTemplateData(self.request)

        currentTimeAsEpochTime = int(time.mktime(time.localtime()))
        templateData['currentTimeAsEpochTime'] = currentTimeAsEpochTime
        logging.debug('currentTimeAsEpochTime: [%s] contestStartTimeAsEpochTime: [%s] contestEndTimeAsEpochTime: [%s]' % \
                        (currentTimeAsEpochTime, templateData['contestStartTimeAsEpochTime'], templateData['contestEndTimeAsEpochTime']))

        numberOfReleasedQuestions = 0
        if currentTimeAsEpochTime >= templateData['contestEndTimeAsEpochTime']:
            numberOfReleasedQuestions = templateData['numberOfQuestions']
            templateData['timeToNextInterval'] = 0
        if currentTimeAsEpochTime > templateData['contestStartTimeAsEpochTime']:
            if numberOfReleasedQuestions is 0:
                numberOfReleasedQuestions =  1 + int((currentTimeAsEpochTime - int(templateData['contestStartTimeAsEpochTime'])) / (60 * int(templateData['questionReleaseIntervalInMinutes'])))
                templateData['timeToNextInterval'] = int(((numberOfReleasedQuestions * int(templateData['questionReleaseIntervalInMinutes'])) * 60) + \
                                                    templateData['contestStartTimeAsEpochTime'] - currentTimeAsEpochTime)
            questionData = {}
            for i in xrange(1, numberOfReleasedQuestions + 1):
                questionCode = 'q'+str(i)
                if questionCode in questionDataConfiguration:
                    questionData[questionCode] = questionDataConfiguration[questionCode]
            templateData['questionData'] = str(questionData)
            logging.debug('numberOfReleasedQuestions: [%s] timeToNextInterval: [%s]' % (numberOfReleasedQuestions, templateData['timeToNextInterval']))

        templateData['numberOfReleasedQuestions'] = numberOfReleasedQuestions

        currentTeamName = ''
        currentTeamPoints = 0
        currentTeamTimeTaken = 0
        if templateData['teamCode'] in teamDataConfiguration:
            currentTeamName = teamDataConfiguration[templateData['teamCode']]['teamName']
            logging.debug('Current teamName is [%s]' % currentTeamName)
            currentTeamPoints = teamDataConfiguration[templateData['teamCode']]['scoreData']['points']
            currentTeamTimeTaken = teamDataConfiguration[templateData['teamCode']]['scoreData']['timeTaken']

        templateData['currentTeamName'] = currentTeamName
        templateData['currentTeamPoints'] = currentTeamPoints
        templateData['currentTeamTimeTaken'] = currentTeamTimeTaken

        try:
            currentQuestionIndex = int(self.request.get('qIdx'), 0)
        except Exception, e:
            currentQuestionIndex = numberOfReleasedQuestions
        templateData['currentQuestionIndex'] = currentQuestionIndex

        # Render the page
        self.render_response('questionsPage.html', **templateData)'''



