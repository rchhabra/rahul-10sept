import sqlite3
import sys
from Question import Question
from Team import Team

class SQLiteUtil:

	def executeInsertUpdateQuery(self,db,query):
		self.conn = sqlite3.connect(db)
		self.c = self.conn.cursor()
		self.c.execute(query)

		self.conn.commit()

		# We can also close the cursor if we are done with it
		self.c.close()

	def getQuestionByIdQuery(self,db,query):
		self.conn = sqlite3.connect(db)
		self.c = self.conn.cursor()
		self.c.execute(query)
		for row in self.c:
		  question_id, question, answer,test_case_file_path  = row
		  quest = Question(question_id, question, answer,test_case_file_path)

		self.conn.commit()

		# We can also close the cursor if we are done with it
		self.c.close()
		return quest

	def getTeamDetailsByNameQuery(self,db,query):
		self.conn = sqlite3.connect(db)
		self.c = self.conn.cursor()
		self.c.execute(query)
		for row in self.c:
		  team_name,team_token,team_score,team_current_question,team_total_time_taken  = row
		  team = Team(team_name,team_token,team_score,team_current_question,team_total_time_taken)

		self.conn.commit()

		# We can also close the cursor if we are done with it
		self.c.close()
		return team

_dbPath ='C:/Users/rahul/codathon/Question_team/Test.sqlite'
x = SQLiteUtil()
quest = x.getQuestionByIdQuery(_dbPath,'select * from Question where question_id = 1')
'''quest = x.getQuestionByIdQuery('F:\Python_testPrograms\Test.sqlite','select * from Question where question_id = 1')'''
'''team = x.getTeamDetailsByNameQuery('F:\Python_testPrograms\Test.sqlite','select * from Team where  team_name = "GS1"')'''
team = x.getTeamDetailsByNameQuery(_dbPath,'select * from Team where  team_name = "GS1"')
print( quest.get_question_id() )
print( quest.get_question() )
print( quest.get_answer() )
print( quest.get_test_case_file_path() )
print(team.get_team_name() )
print(team.get_team_token() )
print(team.get_team_score() )
print(team.get_team_current_question() )
print(team.get_team_total_time_taken() )
